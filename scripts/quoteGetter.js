/**
 * Assignment 3 - Ass3
 * 
 * @author Danilo Zhu 1943382
 * @version 1.01
 */

document.addEventListener('DOMContentLoaded', setup); // The Event Listener for the DOMContentLoaded event. Calls upon the setup() function.

/**
 * This function starts once the DOM content loads, it adds an Event Listener on the button to get quotes, 
 * then once that button is clicked, it calls upon the fetchQuote(e) function.
 */
function setup() {
    const quotebtn = document.querySelector('#quoteGetter');
    quotebtn.addEventListener('click', fetchQuote);
}

/**
 * This function fetches from the API, sends the quote to be shown in the show(quote) function 
 * and handles any errors that occur.
 * 
 * @param {event} e The click event.
 */
function fetchQuote(e) {
    let quoteURL = 'https://ron-swanson-quotes.herokuapp.com/v2/quotes';
    fetch(quoteURL)
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Expected a status code 200, got a: ' + response.status);
            }
        })
        .then(quote => show(quote))
        .catch(error => console.log(error));
}

/**
 * This function gets the div element where the quote will be displayed,
 * deletes the existing quote if there is one, and displays the newly gotten quote.
 * 
 * @param {json} quote The quote JSON coming from the API.
 */
function show(quote) {
    let quoteDiv = document.querySelector('#quoteDiv');
    if (quoteDiv.childNodes.length === 1) {
        let quotePara = document.createElement('P');
        quoteDiv.appendChild(quotePara);
        quotePara.textContent = '"' + quote + '"';
    } else {
        let child = quoteDiv.firstElementChild;
        while (child) {
            child.remove();
            child = quoteDiv.firstElementChild;
        }
        
        let quotePara = document.createElement('P');
        quoteDiv.appendChild(quotePara);
        quotePara.textContent = '"' + quote + '"';
    }
}